create table PMO_Project_Type
(
	Id int primary key,
	[Name] varchar(30)
)
create table PMO_Project_Priority
(
	Id int primary key,
	[Name] varchar(30)
)
create table PMO_Project_Status
(
	Id int primary key,
	[Name] varchar(30)
)
create table PMO_Origin
(
	Id int primary key,
	[Name] varchar(30)
)
create table PMO_Site
(
	Id int primary key,
	[Name] varchar(30)
)
create table PMO_Area
(
	Id int identity primary key,
	[Name] varchar(30)
)
create table PMO_User
(
	Id int identity primary key,
	[Name] varchar(30)
)
create table PMO_Group
(
	Id varchar(5) primary key,
	[Name] varchar(30)
)
create table PMO_Group_User
(
	PMO_Group_UserID int IDENTITY,
	GroupID varchar(5) references PMO_Group(Id),
	UserID int references PMO_User(Id),
	PRIMARY KEY (GroupID, UserID)
)
drop table PMO_Group_User
create table PMO_Customer
(
	Id int identity primary key,
	Name varchar(30)
)
create table PMO_Projects
(
	Id int identity primary key,
	TypeId int references PMO_Project_Type(Id),
	PriorityId int references PMO_Project_Priority(Id),
	StatusId int references PMO_Project_Status(Id),
	PMO int,
	User_Ticket varchar(15),
	FIT_Ticket varchar(15),
	Task int,
	Revision int,
	[Subject] varchar(80),
	[Description] varchar(max),
	OriginId int references PMO_Origin(Id),
	SiteId int references PMO_Site(Id),
	AreaId int references PMO_Area(Id),
	CustomerId int references PMO_Customer(Id),
	GroupId varchar(5) references PMO_Group(Id),
	[Start_Date] datetime,
	Expect_Delivery_Date datetime,
	[User] varchar(8),
	Req_Date datetime,
	Assign varchar(8),
	Documentation varchar(max)
)
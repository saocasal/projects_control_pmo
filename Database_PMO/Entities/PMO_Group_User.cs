
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Database_PMO.Entities
{
    public partial class PMO_Group_User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PMO_Group_UserID { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(5)]
        public string GroupID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserID { get; set; }

        public virtual PMO_Group PMO_Group { get; set; }

        public virtual PMO_User PMO_User { get; set; }
    }
}

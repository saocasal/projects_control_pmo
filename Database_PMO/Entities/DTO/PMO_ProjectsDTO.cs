
using Database_PMO.Entities.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Database_PMO.Entities.DTO
{
    public class PMO_ProjectsDTO
    {
        public int Id { get; set; }

        public int? TypeId { get; set; }

        public int? PriorityId { get; set; }

        public int? StatusId { get; set; }

        public int? PMO { get; set; }

        [StringLength(15)]
        public string User_Ticket { get; set; }

        [StringLength(15)]
        public string FIT_Ticket { get; set; }

        public int? Task { get; set; }

        public int? Revision { get; set; }

        [StringLength(80)]
        public string Subject { get; set; }

        public string Description { get; set; }

        public int? OriginId { get; set; }

        public int? SiteId { get; set; }

        public int? AreaId { get; set; }

        public int? CustomerId { get; set; }

        [StringLength(5)]
        public string GroupId { get; set; }

        public DateTime? Start_Date { get; set; }

        public DateTime? Expect_Delivery_Date { get; set; }

        [StringLength(8)]
        public string User { get; set; }

        public DateTime? Req_Date { get; set; }

        [StringLength(8)]
        public string Assign { get; set; }

        public string Documentation { get; set; }
        public PMO_Project_StatusDTO PMO_Project_Status { get; set; }
        public PMO_Project_PriorityDTO PMO_Project_Priority { get; set; }
        public PMO_CustomerDTO PMO_Customer { get; set; }
        public PMO_GroupDTO PMO_Group { get; set; }
        public PMO_SiteDTO PMO_Site { get; set; }
    }
}

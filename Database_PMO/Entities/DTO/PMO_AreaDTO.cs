
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Database_PMO.Entities.DTO
{
    public class PMO_AreaDTO
    {
        public int Id { get; set; }

        [StringLength(30)]
        public string Name { get; set; }
    }
}

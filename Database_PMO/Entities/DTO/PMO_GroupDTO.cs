
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Database_PMO.Entities.DTO
{
    public class PMO_GroupDTO
    {
        [StringLength(5)]
        public string Id { get; set; }

        [StringLength(30)]
        public string Name { get; set; }
        public List<PMO_UserDTO> PMO_UserDTO { get; set; }
        public List<PMO_ProjectsDTO> Projects { get; set; }
    }
}

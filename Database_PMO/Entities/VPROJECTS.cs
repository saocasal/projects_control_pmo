namespace Database_PMO.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BR1TUSER.VPROJECTS")]
    public partial class VPROJECTS
    {
        [Key]
        [Column(Order = 0)]
        public decimal ACODE { get; set; }
        
        [Column(Order = 1)]
        public string ATYPE { get; set; }
        
        [Column(Order = 2)]
        public decimal PRIORITY { get; set; }
        
        [Column(Order = 3)]
        public string STATUS { get; set; }
        
        [Column("PMO#", Order = 4)]
        public decimal PMO_ { get; set; }
        
        [Column(Order = 5)]
        [StringLength(15)]
        public string USERCASE { get; set; }
        
        [Column(Order = 6)]
        [StringLength(15)]
        public string FITCASE { get; set; }
        
        [Column(Order = 7)]
        [StringLength(80)]
        public string SUBJECT { get; set; }

        [StringLength(302)]
        public string ADESCRIPTION { get; set; }
        
        [Column(Order = 8)]
        [StringLength(5)]
        public string AGROUP { get; set; }
        
        [Column(Order = 9)]
        [StringLength(16)]
        public string REQUESTOR { get; set; }
        
        [Column(Order = 10)]
        public string STARTDATE { get; set; }
        
        [Column(Order = 11)]
        public string ENDDATE { get; set; }
        
        [Column(Order = 12)]
        public string ASITE { get; set; }
    }
}

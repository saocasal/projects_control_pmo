namespace Database_PMO.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PMO_Projects
    {
        public int Id { get; set; }

        public int? TypeId { get; set; }

        public int? PriorityId { get; set; }

        public int? StatusId { get; set; }

        public int? PMO { get; set; }

        [StringLength(15)]
        public string User_Ticket { get; set; }

        [StringLength(15)]
        public string FIT_Ticket { get; set; }

        public int? Task { get; set; }

        public int? Revision { get; set; }

        [StringLength(80)]
        public string Subject { get; set; }

        public string Description { get; set; }

        public int? OriginId { get; set; }

        public int? SiteId { get; set; }

        public int? AreaId { get; set; }

        public int? CustomerId { get; set; }

        [StringLength(5)]
        public string GroupId { get; set; }

        public DateTime? Start_Date { get; set; }

        public DateTime? Expect_Delivery_Date { get; set; }

        [StringLength(8)]
        public string User { get; set; }

        public DateTime? Req_Date { get; set; }

        [StringLength(8)]
        public string Assign { get; set; }

        public string Documentation { get; set; }

        public virtual PMO_Area PMO_Area { get; set; }

        public virtual PMO_Customer PMO_Customer { get; set; }

        public virtual PMO_Group PMO_Group { get; set; }

        public virtual PMO_Origin PMO_Origin { get; set; }

        public virtual PMO_Project_Priority PMO_Project_Priority { get; set; }

        public virtual PMO_Project_Status PMO_Project_Status { get; set; }

        public virtual PMO_Project_Type PMO_Project_Type { get; set; }

        public virtual PMO_Site PMO_Site { get; set; }
    }
}

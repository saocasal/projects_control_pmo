namespace Database_PMO.Context
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Entities;

    public partial class Context_PMO : DbContext
    {
        public Context_PMO()
            : base("name=Context_PMO")
        {
        }

        public virtual DbSet<PMO_Area> PMO_Area { get; set; }
        public virtual DbSet<PMO_Customer> PMO_Customer { get; set; }
        public virtual DbSet<PMO_Group> PMO_Group { get; set; }
        public virtual DbSet<PMO_Origin> PMO_Origin { get; set; }
        public virtual DbSet<PMO_Project_Priority> PMO_Project_Priority { get; set; }
        public virtual DbSet<PMO_Project_Status> PMO_Project_Status { get; set; }
        public virtual DbSet<PMO_Project_Type> PMO_Project_Type { get; set; }
        public virtual DbSet<PMO_Projects> PMO_Projects { get; set; }
        public virtual DbSet<PMO_Site> PMO_Site { get; set; }
        public virtual DbSet<PMO_User> PMO_User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PMO_Area>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Area>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Area)
                .HasForeignKey(e => e.AreaId);

            modelBuilder.Entity<PMO_Customer>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Customer>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Customer)
                .HasForeignKey(e => e.CustomerId);

            modelBuilder.Entity<PMO_Group>()
               .Property(e => e.Id)
               .IsUnicode(false);

            modelBuilder.Entity<PMO_Group>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Group>()
                .HasMany(e => e.PMO_Group_User)
                .WithRequired(e => e.PMO_Group)
                .HasForeignKey(e => e.GroupID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PMO_Group>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Group)
                .HasForeignKey(e => e.GroupId);

            modelBuilder.Entity<PMO_Group_User>()
                .Property(e => e.GroupID)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Origin>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Origin>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Origin)
                .HasForeignKey(e => e.OriginId);

            modelBuilder.Entity<PMO_Project_Priority>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Project_Priority>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Project_Priority)
                .HasForeignKey(e => e.PriorityId);

            modelBuilder.Entity<PMO_Project_Status>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Project_Status>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Project_Status)
                .HasForeignKey(e => e.StatusId);

            modelBuilder.Entity<PMO_Project_Type>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Project_Type>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Project_Type)
                .HasForeignKey(e => e.TypeId);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.User_Ticket)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.FIT_Ticket)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.GroupId)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.User)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.Assign)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Projects>()
                .Property(e => e.Documentation)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Site>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_Site>()
                .HasMany(e => e.PMO_Projects)
                .WithOptional(e => e.PMO_Site)
                .HasForeignKey(e => e.SiteId);

            modelBuilder.Entity<PMO_User>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PMO_User>()
                .HasMany(e => e.PMO_Group_User)
                .WithRequired(e => e.PMO_User)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete(false);
        }
    }
}

namespace Database_PMO.Context
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Database_PMO.Entities;

    public partial class Context_PMO_Oracle : DbContext
    {
        public Context_PMO_Oracle()
            : base("name=OracleBaan")
        {
            Database.SetInitializer<Context_PMO_Oracle>(null);
        }

        public virtual DbSet<VPROJECTS> VPROJECTS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.ACODE)
                .HasPrecision(38, 0);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.ATYPE)
                .IsUnicode(false);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.PRIORITY)
                .HasPrecision(38, 0);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.STATUS);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.PMO_)
                .HasPrecision(38, 0);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.USERCASE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.FITCASE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.SUBJECT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.ADESCRIPTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.AGROUP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.REQUESTOR)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<VPROJECTS>()
                .Property(e => e.ASITE)
                .IsUnicode(false);
        }
    }
}

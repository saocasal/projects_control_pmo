﻿$(document).ready(function () {
    var x = location.search.split("?")
    if (x.length > 0)
    {
        get_project(x[1].replace("id=",""))
    }

    $('#btnSave').click(function () {
        DTO = {
                ACODE: $('#Code').val(),
                ATYPE: $('#proj_type').val(),
                PRIORITY: $('#prio').val(),
                STATUS: $('#stat').val(),
                PMO_: $('#PMOId').val(),
                USERCASE: $('#UserTicket').val(),
                FITCASE: $('#FITTicket').val(),
                SUBJECT: $('#Subject').val(),
                ADESCRIPTION: $('#Description').val(),
                ASITE: $('#site').val(),
                AGROUP: $('#group').val(),
                STARTDATE: $('#Startdate').val(),
                ENDDATE: $('#Enddate').val(),
                REQUESTOR: $('#requestor').val()
            };
        
        save_project(DTO)
    });
});
function save_project(DTO) {
    a = JSON.stringify({ p: DTO })
    complete_ajax_with_parameters_without_success("POST", "../PMO_Projects/SaveProject", "application/json;charset=utf-8", JSON.stringify({ p: DTO })).done((data) => {

        if (data)
            alert(data);
        else  
            window.location.href = "/PMO_Projects/Index";
    });
}
function get_project(idtable) {
    complete_ajax_with_parameters_without_success("POST", "../PMO_Projects/Project", "application/json;charset=utf-8", JSON.stringify({ id: idtable })).done((data) => {

        var r = JSON.parse(data);
        $("#project").html(
            `<div class="form-group">
                    <label>Code</label>
                    <input type="text" disabled class ="form-control" id="Code" value = '${r.ACODE}'>
                </div>
                <div class ="form-group">
                    <label>Project Type</label>
                    <div id="project_type">
                        <select id="proj_type" class ="form-control">
                            <option value="Project">Project</option>
                            <option value="Small Project">Small Project</option>
                            <option value="BugFix">BugFix</option>
                            <option value="Patch">Patch</option>
                            <option value="Task">Task</option>
                        </select>
                    </div>
                </div>
                <div class ="form-group">
                    <label>Priority</label>
                    <div id="priority">
                        <select id="prio" class ="form-control">
                            <option value="50">P5 -Minimum</option>
                            <option value="40">P4 -Low</option>
                            <option value="30">P3 -Normal</option>
                            <option value="20">P2 -High</option>
                            <option value="10">P1 -Critical</option>
                        </select>
                    </div>
                </div>
                <div class ="form-group">
                    <label>Status</label>
                    <div id="status">
                        <select id="stat" class ="form-control">
                            <option value="New">New</option>
                            <option value="On Analysis">On Analysis</option>
                            <option value="Scope">Scope</option>
                            <option value="Waiting User">Waiting User</option>
                            <option value="Spec">Spec</option>
                            <option value="Development">Development</option>
                            <option value="Retrofit">Retrofit</option>
                            <option value="Test">Test</option>
                            <option value="IS Test">IS Test</option>
                            <option value="UAT">UAT</option>
                            <option value="Documentation">Documentation</option>
                            <option value="To Install">To Install</option>
                            <option value="Dependency">Dependency</option>
                            <option value="Installed">Installed</option>
                            <option value="Dump">Dump</option>
                            <option value="On Hold">On Hold</option>
                            <option value="Cancelled">Cancelled</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>PMO #</label>
                    <input type="text" class ="form-control" placeholder="PMO #" id="PMOId" value = '${r.PMO_}'>
                </div>
                <div class="form-group">
                    <label>User Ticket</label>
                    <input type="text" class ="form-control" placeholder="REQ" id="UserTicket" value = '${r.USERCASE == null ? "" :r.USERCASE}'>
                </div>
                <div class="form-group">
                    <label>FIT Ticket</label>
                    <input type="text" class ="form-control" placeholder="RITM" id="FITTicket" value = '${r.FITCASE == null ? "" :r.FITCASE}'>
                </div>
                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" class ="form-control" placeholder="Subject" id="Subject" value = '${r.SUBJECT == null ? "" :r.SUBJECT}'>
                </div>
                <div class="form-group">
                    <label>Short Description</label>
                    <textarea class ="form-control" rows="6" placeholder="Description" id="Description">${r.ADESCRIPTION == null ? "": r.ADESCRIPTION}</textarea>
                </div>

                <div class="form-group">
                    <label>Site</label>
                    <div id="site_">
                        <select id="site" class ="form-control">
                            <option value="Todos">Todos</option>
                            <option value="Sorocaba">Sorocaba</option>
                            <option value="Jaguariuna">Jaguariuna</option>
                            <option value="Masa Eletronics">Masa Eletronics</option>
                            <option value="Masa Plastics">Masa Plastics</option>
                            <option value="FIT">FIT</option>
                            <option value="Sinctronics">Sinctronics</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label>Group</label>
                    <div id="groupdiv">
                        <input type="text" class ="form-control" placeholder="Group" id="group" value = '${r.AGROUP == null ? "" :r.AGROUP}'>
                    </div>
                </div>

                <div class="form-group">
                    <label>Start Date:</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input type="text" id="Startdate" value = '${r.STARTDATE == "00/00/0000" ? "" :r.STARTDATE}' class ="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                    </div>
                </div>


                <div class ="form-group">
                    <label>Expected Delivery Date: </label>
                    <div class ="input-group">
                        <div class ="input-group-prepend">
                            <span class ="input-group-text"><i class ="fa fa-calendar"></i></span>
                        </div>
                        <input type="text" id="Enddate" value = '${r.ENDDATE == "00/00/0000" ? "" :r.ENDDATE}' class ="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                    </div>
                </div>

                <div class ="form-group">
                    <label>Requestor</label>
                    <input type="text" class ="form-control" placeholder="Login" id="requestor" value = '${r.REQUESTOR == null ? "" :r.REQUESTOR}'>
                </div>`
        );

        //Datemask dd/mm/yyyy
        $('#Startdate').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#Enddate').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()
        if (r.ACODE != 0)
        {
            $('#proj_type').val(r.ATYPE);
            $('#prio').val(r.PRIORITY);
            $('#stat').val(r.STATUS);
            $('#site').val(r.ASITE);
        }
        else
        {
            $('#stat').val("New");
            $('#site').val("Todos");
        }
    });
}
//<div class="form-group">
//                    <label>Business área</label>
//                    <div id="ba">
//                        <input type="text" class ="form-control" placeholder="Group" id="group" value = ${r.AREA}>
//                    </div>
//                </div>

//                <div class="form-group">
//                    <label>Customer</label>
//                    <div id="customer">
//                        <input type="text" class ="form-control" placeholder="Group" id="group" value = ${r.CUSTOMER}>
//                    </div>
//                </div>
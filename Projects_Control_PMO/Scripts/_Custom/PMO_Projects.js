﻿$(document).ready(function () {
    read_dashboard();
});
$(document).on("focusout", 'input[type="search"]', function () {
    sessionStorage.setItem("Search", $(this).val());
});
$(document).on("dblclick", "tr", function () {
    var id = $(this).attr('id');
    window.location.href = "/PMO_Projects/Project?id=" + id;
});
function read_dashboard() {
    complete_ajax_without_parameters_and_success("GET", "../PMO_Projects/GetAllProjects", "application/json;charset=utf-8").done((data) => {

        var response = JSON.parse(data);

        if (response) {
            $("#TableDashboard").html(response.map((r) =>
                `<tr id="${r.ACODE}">
                    <td id="code_id" style="display:none;">${r.ACODE}</td>
                    <td id="type_id">${r.ATYPE}</td>
                    <td id="priority_id">${r.PRIORITY}</td>
                    <td id="status_id">${r.STATUS}</td>
                    <td id="pmo_id">${r.PMO_}</td>
                    <td id="usercase_id">${r.USERCASE}</td>
                    <td id="fitcase_id">${r.FITCASE}</td>
                    <td id="subject_id">${r.SUBJECT}</td>
                    <td id="group_id">${r.AGROUP}</td>
                    <td id="requestor_id">${r.REQUESTOR}</td>
                    <td id="start_date_id">${r.STARTDATE == '00/00/0000' ? '': r.STARTDATE}</td>
                    <td id="exp_delivery_id">${r.ENDDATE == '00/00/0000' ? '': r.ENDDATE}</td>
                    <td id="site_id">${r.ASITE}</td>
                </tr>`
            ));

            $('#example1').DataTable();

            searchval = sessionStorage.getItem("Search");
            if (searchval)
            {
                $('input[type="search"]').val(searchval);
                $('input[type="search"]').trigger('keyup');
            }
        }
    });
}
function formatDate(value) {
    return value.getDate() + "/" + (value.getMonth() + 1) + "/" + value.getFullYear();
}
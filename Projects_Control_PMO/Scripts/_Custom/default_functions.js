﻿$(document).ready(function () {

    var height = window.innerHeight;

    $(".loader").css("height", height);

});

var response;

function complete_ajax_without_parameters_and_success(type, url, contentType) {
    return $.ajax({
        type: type,
        url: url,
        contentType: contentType,
        beforeSend: function () {
            showLoading();
        },
        error: function (e) {
            showError(e.statusText);
        },
        complete: function () {
            hideLoading();
        }
    });
}

function complete_ajax_with_parameters_without_success(type, url, contentType, parameters) {
    return $.ajax({
        type: type,
        url: url,
        contentType: contentType,
        data: parameters,
        beforeSend: function () {
            showLoading();
        },
        error: function (e) {
            showError(e.statusText);
        },
        complete: function () {
            hideLoading();
        }
    });
}

function showLoading() {
    $(".loader").removeClass("d-none");
}

function hideLoading() {
    $(".loader").addClass("d-none");
}

function showError() {
    $("#msg_div").html("Something is wrong,<br> reload the page");
    $('.modal-message').modal('show');
}

function showNotificationMessage(message) {
    $("#notification-message").html(message);
    $("#notification-message").parent().removeClass("hidden");
}

Array.prototype.contains = function (v) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === v) return true;
    }
    return false;
};

Array.prototype.unique = function () {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        if (!arr.includes(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr;
};


$(document).on("click", "#logout-button", function () {

    complete_ajax_without_parameters_and_success("GET", "../Login/Logout", "application/json;charset=utf-8").done(function (data) {
        window.location.href = "../Login/Index";
    });

});

function EncodeBase64Utf8(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
}

function DecodeBase64Utf8(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

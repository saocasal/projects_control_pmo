﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Database_PMO.Context;
using Database_PMO.Entities;
using Projects_Control_PMO.DAO.Repository;

namespace Projects_Control_PMO.DAO
{
    public class VPROJECTS_DAO : RepositoryBaseOracle<VPROJECTS>
    {
        public List<VPROJECTS> GetProjectsByListGroup(List<PMO_Group> groups)
        {
            List<VPROJECTS> list = new List<VPROJECTS>();

            using (var context = new Context_PMO_Oracle())
            {
                foreach (var item in groups)
                {
                    list.AddRange(context.VPROJECTS.Where(p => p.AGROUP == item.Id).ToList());
                }
            }

            return (list);
        }
    }
}
﻿using Database_PMO.Context;
using Database_PMO.Entities;
using Projects_Control_PMO.DAO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projects_Control_PMO.DAO
{
    public class PMO_User_DAO : RepositoryBase<PMO_User>
    {
        public PMO_User GetGroupsByUser(string name)
        {
            PMO_User user = new PMO_User();

            using (var context = new Context_PMO())
            {
                user = context.PMO_User.Where(p => p.Name == name).FirstOrDefault();
            }

            return (user);
        }
    }
}
﻿using Database_PMO.Entities;
using Projects_Control_PMO.DAO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Database_PMO.Context;

namespace Projects_Control_PMO.DAO
{
    public class PMO_Group_DAO : RepositoryBase<PMO_Group>
    {
        public List<PMO_Group> GetGroupsByUser(PMO_User user)
        {
            List<PMO_Group> list = new List<PMO_Group>();

            using (var context = new Context_PMO())
            {
                context.Configuration.LazyLoadingEnabled = false;
                list = context.PMO_Group.ToList();
                //list = context.PMO_Group.Include(g => g.PMO_User).ToList();
            }

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].PMO_Group_User.FirstOrDefault().UserID == user.Id)
                {
                    list.RemoveAt(i);
                }
            }

            return (list);
        }
    }
}
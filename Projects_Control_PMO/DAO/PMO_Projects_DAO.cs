﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Database_PMO.Context;
using Database_PMO.Entities;
using Projects_Control_PMO.DAO.Repository;

namespace Projects_Control_PMO.DAO
{
    public class PMO_Projects_DAO : RepositoryBase<PMO_Projects>
    {
        public List<PMO_Projects> GetProjectsByGroup(string GroupId)
        {
            List<PMO_Projects> list = new List<PMO_Projects>();

            using (var context = new Context_PMO())
            {
                list = context.PMO_Projects.Where(p => p.GroupId == GroupId).ToList();
            }

            return (list);
        }
    }
}
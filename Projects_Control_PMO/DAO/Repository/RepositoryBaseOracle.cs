﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Database_PMO.Context;
using System.Data.Entity;

namespace Projects_Control_PMO.DAO.Repository
{
    public class RepositoryBaseOracle<TEntity> : IDisposable where TEntity : class
    {
        protected Context_PMO_Oracle wmc = new Context_PMO_Oracle();
        public void Add(TEntity obj)
        {
            wmc.Set<TEntity>().Add(obj);
            wmc.SaveChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return wmc.Set<TEntity>().ToList();
        }

        public TEntity GetById(int id)
        {
            return wmc.Set<TEntity>().Find(id);
        }

        public void Remove(int id)
        {
            wmc.Set<TEntity>().Remove(GetById(id));
            wmc.SaveChanges();
        }

        public void Update(TEntity obj)
        {
            wmc.Entry(obj).State = EntityState.Modified;
            wmc.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
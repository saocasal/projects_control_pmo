﻿using AutoMapper;
using Database_PMO.Entities;
using Database_PMO.Entities.DTO;
using Newtonsoft.Json;
using Projects_Control_PMO.BaanInterface;
using Projects_Control_PMO.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projects_Control_PMO.Controllers
{
    public class PMO_ProjectsController : Controller
    {
        private PMO_Projects_DAO ProjectsDAO = new PMO_Projects_DAO();
        private PMO_User_DAO UserDAO = new PMO_User_DAO();
        private PMO_Group_DAO GroupDAO = new PMO_Group_DAO();
        private VPROJECTS_DAO a = new VPROJECTS_DAO();
        private BaaNInterfaceClient baan = new BaaNInterfaceClient();

        // GET: PMO_Projects
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string SaveProject(VPROJECTSDTO p)
        {
            CleanNullValues(ref p);
            return baan.ExecBaaNFunction("ogdpmodll0001", "gdpmodll0001.save.project.web(" +
                                                          p.ACODE + "," +
                                                          '"' + p.ATYPE.Trim() + '"' + "," +
                                                          '"' + p.PRIORITY + '"' + "," +
                                                          '"' + p.STATUS.Trim() + '"' + "," +
                                                          p.PMO_ + "," +
                                                          '"' + p.USERCASE.Trim() + '"' + "," +
                                                          '"' + p.FITCASE.Trim() + '"' + "," +
                                                          '"' + p.SUBJECT.Trim() + '"' + "," +
                                                          '"' + p.ADESCRIPTION.Trim() + '"' + "," +
                                                          '"' + p.ASITE.Trim() + '"' + "," +
                                                          '"' + p.AGROUP.Trim() + '"' + "," +
                                                          '"' + p.STARTDATE.Trim() + '"' + "," +
                                                          '"' + p.ENDDATE.Trim() + '"' + "," +
                                                          '"' + p.REQUESTOR.Trim() + '"' +
                                                          ")");
        }
        private void CleanNullValues(ref VPROJECTSDTO p)
        {
            if (p.ADESCRIPTION == null)
                p.ADESCRIPTION = "";
            if (p.ATYPE == null)
                p.ATYPE = "";
            if (p.USERCASE == null)
                p.USERCASE = "";
            if (p.FITCASE == null)
                p.FITCASE = "";
            if (p.SUBJECT == null)
                p.SUBJECT = "";
            if (p.ASITE == null)
                p.ASITE = "";
            if (p.AGROUP == null)
                p.AGROUP = "";
            if (p.STARTDATE == null)
                p.STARTDATE = "";
            if (p.ENDDATE == null)
                p.ENDDATE = "";
        }
        // POST: Project
        [HttpPost]
        public string Project(int? id)
        {
            VPROJECTSDTO p;

            if (id != 0)
                p = VProjectsToDTO(a.GetById(id.Value));
            else
                p = new VPROJECTSDTO();

            return JsonConvert.SerializeObject(p);
        }
        public ActionResult Project()
        {
            return View();
        }
        [HttpGet]
        public string GetAllProjects()
        {
            return JsonConvert.SerializeObject(VProjectsToDTO(a.GetAll().ToList()));
        }
        [HttpGet]
        public string GetProjectsByUser()
        {
            string user = "saocasal";

            List<PMO_Projects> projects = new List<PMO_Projects>();

            foreach (var item in GroupDAO.GetGroupsByUser(UserDAO.GetGroupsByUser(user)))
            {
                projects.AddRange(ProjectsDAO.GetProjectsByGroup(item.Id));
            }

            return JsonConvert.SerializeObject(ProjectsToDTO(projects));
        }
        public List<PMO_ProjectsDTO> ProjectsToDTO(List<PMO_Projects> projects)
        {
            List<PMO_ProjectsDTO> ProjectsDTO = Mapper.Map<List<PMO_Projects>, List<PMO_ProjectsDTO>>(projects);

            return (ProjectsDTO);
        }
        public List<VPROJECTSDTO> VProjectsToDTO(List<VPROJECTS> vprojects)
        {
            List<VPROJECTSDTO> VProjectsDTO = Mapper.Map<List<VPROJECTS>, List<VPROJECTSDTO>>(vprojects);

            return (VProjectsDTO);
        }
        public VPROJECTSDTO VProjectsToDTO(VPROJECTS vprojects)
        {
            VPROJECTSDTO VProjectsDTO = Mapper.Map<VPROJECTS, VPROJECTSDTO>(vprojects);

            return (VProjectsDTO);
        }
    }
}
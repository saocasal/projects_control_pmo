﻿using System.Web;
using System.Web.Mvc;

namespace Projects_Control_PMO
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

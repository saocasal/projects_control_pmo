﻿using AutoMapper;
using Database_PMO.Entities;
using Database_PMO.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects_Control_PMO.Mappers
{
    class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewToDomainModelMappings"; }
        }
        public DomainToViewModelMappingProfile()
        {
            CreateMap<PMO_Projects, PMO_ProjectsDTO>();
            CreateMap<PMO_Area, PMO_AreaDTO>();
            CreateMap<PMO_Customer, PMO_CustomerDTO>();
            CreateMap<PMO_Group, PMO_GroupDTO>();
            CreateMap<PMO_Origin, PMO_OriginDTO>();
            CreateMap<PMO_Project_Priority, PMO_Project_PriorityDTO>();
            CreateMap<PMO_Project_Status, PMO_Project_StatusDTO>();
            CreateMap<PMO_Site, PMO_SiteDTO>();
            CreateMap<PMO_User, PMO_UserDTO>();
            CreateMap<PMO_Group_User, PMO_Group_UserDTO>();
            CreateMap<VPROJECTS, VPROJECTSDTO>();
        }
    }
}

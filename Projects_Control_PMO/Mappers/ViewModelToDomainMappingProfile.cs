﻿using AutoMapper;
using Database_PMO.Entities;
using Database_PMO.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects_Control_PMO.Mappers
{
    class ViewModelToDomainMappingProfile :Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<PMO_ProjectsDTO, PMO_Projects>();
            CreateMap<PMO_AreaDTO, PMO_Area>();
            CreateMap<PMO_CustomerDTO, PMO_Customer>();
            CreateMap<PMO_GroupDTO, PMO_Group>();
            CreateMap<PMO_OriginDTO, PMO_Origin>();
            CreateMap<PMO_Project_PriorityDTO, PMO_Project_Priority>();
            CreateMap<PMO_Project_StatusDTO, PMO_Project_Status>();
            CreateMap<PMO_SiteDTO, PMO_Site>();
            CreateMap<PMO_UserDTO, PMO_User>(); 
            CreateMap<PMO_Group_UserDTO, PMO_Group_User>();
            CreateMap<VPROJECTSDTO, VPROJECTS>();
        }
    }
}

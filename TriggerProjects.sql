-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Alter TRIGGER CancelEmptyValues
   ON  PMO_Projects
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	/*
	Declare @Origin Int;
	Declare @Site Int;
	Declare @Area Int;

	select @Area = AreaId, @Site = SiteId, @Origin = OriginId from inserted

	if CONVERT(varchar(10), @Area) = ''
	BEGIN
		
	END*/
	UPDATE PMO_Projects
    SET PMO_Projects.AreaId = replace(PMO_Projects.AreaId ,'',null),
		PMO_Projects.OriginId = 1,
		PMO_Projects.SiteId = replace(PMO_Projects.SiteId ,'',null)
    FROM PMO_Projects
    INNER JOIN inserted i on i.Id= PMO_Projects.Id
    
END
GO
Create TRIGGER CancelEmptyValuesBefore
   ON  PMO_Projects
   instead of INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	/*
	Declare @Origin Int;
	Declare @Site Int;
	Declare @Area Int;

	select @Area = AreaId, @Site = SiteId, @Origin = OriginId from inserted

	if CONVERT(varchar(10), @Area) = ''
	BEGIN
		
	END*/
	UPDATE PMO_Projects
    SET PMO_Projects.AreaId = replace(PMO_Projects.AreaId ,'',null),
		PMO_Projects.OriginId = 1,
		PMO_Projects.SiteId = replace(PMO_Projects.SiteId ,'',null)
    FROM PMO_Projects
    INNER JOIN inserted i on i.Id= PMO_Projects.Id
    
END
GO

ALTER TABLE [dbo].PMO_Projects ENABLE TRIGGER [CancelEmptyValues]
GO
